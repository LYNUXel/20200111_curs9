import java.util.Scanner;

// Avem un menu de produse afisate (water, cola, fanta)
//    alegerea produsului
//    afisare pret
//    ales 10 => suma
//    alegere gresita 10 , reintoarcere l pasul precedent.
public class Exercise_05 {
    public static Scanner s = new Scanner(System.in);
    public static void displayMessage(String message) {
        System.out.println(message);
    }

    public static int selectProduct() {
        displayMessage("Insert number of the drink: ");
        int x = s.nextInt();
        if (x != 1 && x != 2 && x != 3 && x != 4 && x != 5) {
            displayMessage("ERROAR: Select the product! ");
            System.exit(0);
        }
        return x;
    }

    public static void pay(int price) {
        int y;
        do {
            displayMessage("Just pay " + price + " Ron !");
            y = s.nextInt();
        }
        while (y != price);
        displayMessage("Wait your product...");
    }

    public static int getPrice(int code) {
        switch (code) {
            case 1:
                return 3;
            case 2:
                return 5;
            case 3:
                return 6;
            case 4:
                return 4;
            case 5:
                return 5;
            default:
                return 0;
        }
    }

    public static void main(String[] args) {
        String meniu = "Products List:\n1. Water\n2. Fanta\n3. Coca Cola\n4. Short Espresso\n5. Long Espresso";
        displayMessage(meniu);
        int productCode = selectProduct();
        int price = getPrice(productCode);
        displayMessage("Product price is: " + price + " Ron !");
        pay(price);

        // System.out.println("Introduceti numarul bauturii dorite: ");
        // System.out.println("Bautura aleasa este: " + x);
        // System.out.println("Pretul produsului ales este 10 Ron");
    }
}