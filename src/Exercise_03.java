import java.util.Scanner;

public class Exercise_03 {
    public static void main(String[] args) {
        //citire un sir de numere intregi, cand am ajuns la valoarea 0, sa se opreasca afisarea.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu numere in Arrays: ");

        int[] arr = new int[100];  // marime
        int x = scanner.nextInt(); // introducere numere / citirea de la consola,
                                   // nextInt citirea numerelor din clasa scanner
        int index = 0;             // index = este ultima pozitie in array
        while (x != 0 && index < arr.length) { // cand x este diferit de 0
            arr[index] = x;        // cand
            x = scanner.nextInt();
            index++;
        }
        System.out.print("Arrayul este = [");
        for (int i = 0; i < index; i++) {
            System.out.print(arr[i] + "");
            if (i != index-1){          // cand sunt pe ulitmul element adauga ; intre numerele introduse
                System.out.print("; ");
            }
        }
        System.out.print("]");
    }
}