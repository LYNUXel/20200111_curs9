import java.util.Scanner;

public class Exercise_02 {
    public static void main(String[] args) {
        //citire int de la consola, concateneaza cu string si afisare
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu INT-ul: ");
        int x = scanner.nextInt();

        //scanner.nextLine();
        System.out.print("Introdu textul: ");
        String text = scanner.next();

        System.out.println("Ai introdus INT-ul: " + x + " " + "cu textul: " + text);

    }

}
