import java.util.Scanner;

public class Exercise_04 {
// Creare triunghi de la mic la mare, in functie de nuamrul ales de coloana
public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Introdu numarul de linii/coloana: ");
    int x = scanner.nextInt();
    for (int i = 0; i < x; i++) {
        for (int j = 0; j <= i; j++) {
            System.out.print("* ");
        }
        System.out.println(" ");
    }
}

}